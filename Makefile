CPPFLAGS = -D_FORTIFY_SOURCE=2 -D_DEFAULT_SOURCE
CFLAGS   = $(CPPFLAGS) -std=c99 -pedantic -Wall -Wextra -Werror -Os -march=native -mtune=native -fPIE
LDFLAGS  = -Wl,-z,now,-z,relro,-s,-pie

OBJS = obj/main.o
BIN  = bin/stc
MAN  = man1/stc.1.gz

PREFIX = /usr

default: obj bin $(BIN) $(MAN)

obj:
	@mkdir obj

bin:
	@mkdir bin

$(BIN): $(OBJS)
	$(CC)    -o $@ $^ $(LDFLAGS)

obj/%.o: src/%.c
	$(CC) -c -o $@ $^ $(CFLAGS)

man1/%.1.gz: man1/%.1
	gzip -ck9 $^ > $@

clean:
	@if test -d obj; then  rm -rf obj; fi
	@if test -d bin; then rm -rf bin; fi
	@if test -f $(MAN); then rm -f $(MAN); fi

format:
	@clang-format -i -style="{BasedOnStyle: mozilla, IndentWidth: 4}" src/*.c

install: default
	install -Dm755 $(BIN) $(PREFIX)/bin/stc
	install -Dm644 LICENSE $(PREFIX)/share/licenses/stc/LICENSE
	install -Dm644 man1/stc.1.gz $(PREFIX)/share//man/man1/stc.1.gz

uninstall:
	rm -f $(PREFIX)/bin/stc
	rm -f $(PREFIX)/share/licenses/stc/LICENSE
	rm -f $(PREFIX)/share/man/man1/stc.1.gz

.PHONY: default clean format install uninstall
