![STC logo](logo.png)

## Build

```
git clone https://framagit.org/franck.stauffer/stc
cd stc
make
```

## Install

```
sudo make install
```

## Uninstall

```
sudo make uninstall
```

## Usage

```
stc address|domainname [port]
```
